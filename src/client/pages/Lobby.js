import React from 'react';
import PropTypes from 'prop-types';

import Score from './components/Score';
import Logo from './components/Logo';

const Lobby = ({ teams }) => (
  <div>
    <Score teams={teams} />
    <Logo />
  </div>
);

Lobby.propTypes = {
  teams: PropTypes.arrayOf(PropTypes.shape).isRequired,
};

export default Lobby;
