import React from 'react';
import PropTypes from 'prop-types';

import {
  Button,
  Modal,
  ModalBackground,
  ModalCard,
  ModalCardHeader,
  ModalCardTitle,
  Delete,
  ModalCardBody,
  ModalCardFooter,
  Field,
  Label,
  Control,
  Input,
} from 'bloomer';

class TeamModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.submit = this.submit.bind(this);
  }


  handleChange({ target }) {
    this.setState({
      name: target.value,
    });
  }

  submit() {
    const { addTeam } = this.props;
    const { name } = this.state;
    addTeam(name);
  }


  render() {
    const { isActive, closeModal } = this.props;
    const { name } = this.state;
    return (
      <Modal isActive={isActive}>
        <ModalBackground onClick={closeModal} />
        <ModalCard>
          <ModalCardHeader>
            <ModalCardTitle>Lägg till lag</ModalCardTitle>
            <Delete onClick={closeModal} />
          </ModalCardHeader>
          <ModalCardBody>
            <Field>
              <Label>Namn</Label>
              <Control>
                <Input
                  type="text"
                  placeholder="Lagnamn"
                  onChange={this.handleChange}
                />
              </Control>
            </Field>
          </ModalCardBody>
          <ModalCardFooter>
            <Button disabled={!name.length > 0} onClick={this.submit}>Spara</Button>
            <Button onClick={closeModal}>Avbryt</Button>
          </ModalCardFooter>
        </ModalCard>
      </Modal>
    );
  }
}

TeamModal.propTypes = {
  isActive: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  addTeam: PropTypes.func.isRequired,
};


export default TeamModal;
