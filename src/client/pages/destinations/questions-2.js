import React from 'react';
import {
  Subtitle, Columns, Column,
} from 'bloomer';

const Questions = () => (
  <div className="questions">
    <Columns>
      <Column isSize="1/2">
        <Subtitle hasTextColor="light" isSize={3}>Fråga 1</Subtitle>
        <p>
          Vad heter byggnaden som inrymmer den anatomiska teatern där man
          förr i tiden kunde se offentliga dissektioner?
        </p>
        <hr />
        <Subtitle hasTextColor="light" isSize={3}>Fråga 2</Subtitle>
        <p>Uppsala hette tidigare Östra Aros, men varför bytte staden namn?</p>
        <hr />
      </Column>
      <Column isSize="1/2">
        <Subtitle hasTextColor="light" isSize={3}>Fråga 3</Subtitle>
        <p>Hur många nobelpristagare har anknytning till Uppsala universitet?</p>
        <hr />
        <Subtitle hasTextColor="light" isSize={3}>Fråga 4</Subtitle>
        <p>
          Anders Celsius är född i Uppsala och är mest känd för att ha uppfunnit Celsius-skalan.
          Nämn så många olika temperaturskalor ni kan.
        </p>
        <hr />
      </Column>
    </Columns>
  </div>
);

export default Questions;
