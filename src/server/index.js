const server = require('http').createServer();
const io = require('socket.io')(server);

let teams = [];
let train = false;
let route = '';


function update() {
  io.emit('update', { teams, train, route });
}


server.listen(3000);

io.on('connection', (socket) => {
  update();


  socket.on('add team', (team) => {
    teams.push({
      name: team.name,
      id: team.id,
      points: 0,
      stopped: false,
    });
    update();
  });

  socket.on('remove team', (id) => {
    teams = teams.filter(team => team.id !== id);
    update();
  });

  socket.on('add point', (id) => {
    const team = teams.find(t => t.id === id);
    if (team) {
      team.points += 1;
      update();
    }
  });

  socket.on('remove point', (id) => {
    const team = teams.find(t => t.id === id);
    if (team) {
      team.points -= 1;
      update();
    }
  });

  socket.on('stop team', (id) => {
    if (!train) return;
    const team = teams.find(t => t.id === id);
    if (team) {
      team.stopped = true;
      train = false;
      update();
    }
  });

  socket.on('start team', (id) => {
    const team = teams.find(t => t.id === id);
    if (team) {
      team.stopped = false;
      update();
    }
  });

  socket.on('reset teams', () => {
    teams = teams.map(team => ({ ...team, stopped: false }));
    update();
  });

  socket.on('toggle team', (id) => {
    const team = teams.find(t => t.id === id);
    if (team) {
      team.stopped = !team.stopped;
      update();
    }
  });

  socket.on('toggle train', () => {
    train = !train;
    update();
  });

  socket.on('navigate', (newRoute) => {
    route = newRoute;
    io.emit('navigate', route);
    update();
  });
});
