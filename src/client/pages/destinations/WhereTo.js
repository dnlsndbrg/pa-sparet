import React from 'react';
import PropTypes from 'prop-types';
import Sound from 'react-sound';
import YouTube from 'react-youtube';

import copenhager from '../../assets/copenhager.mp3';
import horn from '../../assets/horn.ogg';
import Logo from '../../assets/img/logo-dark.png';

const opts = {
  controls: 0,
  modestbranding: 1,
  fs: 0,
  width: 960,
  height: 540,
};


class Destination extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isPlaying: props.train,
      playHorn: false,
    };

    this.ytplayer = React.createRef();
    this.horn = React.createRef();
    this.onReady = this.onReady.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    const change = nextState.isPlaying !== nextProps.train;

    if (change) {
      this.setState({
        isPlaying: nextProps.train,
      }, this.forceUpdate);

      if (nextProps.train) {
        this.ytplayer.current.internalPlayer.playVideo();
        this.setState({ playHorn: false });
      } else {
        this.setState({ playHorn: true });
        this.ytplayer.current.internalPlayer.pauseVideo();
      }
    }


    return false;
  }

  onReady() {
    const { isPlaying } = this.state;
    this.ytplayer.current.internalPlayer.setPlaybackRate(1.5);
    if (isPlaying) { this.ytplayer.current.internalPlayer.playVideo(); }
  }

  render() {
    const { isPlaying, playHorn } = this.state;
    const { videoId } = this.props;
    return (
      <div className="destination">
        <Sound url={copenhager} playStatus={isPlaying ? 'PLAYING' : 'PAUSED'} />
        <Sound url={horn} playStatus={playHorn ? 'PLAYING' : 'PAUSED'} />
        <YouTube
          className={`ytplayer ${!isPlaying ? 'ytplayer--paused' : ''}`}
          ref={this.ytplayer}
          opts={opts}
          videoId={videoId}
          onReady={this.onReady}
        />
        {
          !isPlaying && <div className="logo" style={{ backgroundImage: `url(${Logo})` }} />
        }
      </div>
    );
  }
}

Destination.propTypes = {
  train: PropTypes.bool.isRequired,
  videoId: PropTypes.string.isRequired,
};

export default Destination;
