import React from 'react';
import {
  Image, Subtitle, Columns, Column,
} from 'bloomer';

const Questions = () => (
  <div className="questions">
    <Columns>
      <Column isSize="1/2">
        <Subtitle hasTextColor="light" isSize={3}>Fråga 1</Subtitle>
        <p>Vad är det här för maträtt?</p>
        <Image isSize="128x128" src="https://via.placeholder.com/128x128" />
        <hr />
      </Column>
      <Column isSize="1/2">
        <Subtitle hasTextColor="light" isSize={3}>Fråga 2</Subtitle>
        <p>
          På 1600-talet härjade det 30 åriga kriget i Tyskland. Ett religiöst krig där protestanter
          slogs mot katoliker. På vilken sida i kriget stod Hamburg?
        </p>
        <hr />
        <Subtitle hasTextColor="light" isSize={3}>Fråga 3</Subtitle>
        <p>
          Nivea grundades i Hamburg, Människor har länge använt hudvårdsprodukter, vad badade
          Cleopatra i enligt legenden för att bevara sin skönhet?
        </p>
        <hr />
      </Column>
    </Columns>
  </div>

);

export default Questions;
