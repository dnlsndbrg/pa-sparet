import React from 'react';
import PropTypes from 'prop-types';
import shortid from 'shortid';

import {
  Button,
  Title,
} from 'bloomer';

const SelectTeam = (props) => {
  const { teams, selectTeam } = props;
  return (
    <div className="select-team">
      <Title tag="h1">Välj lag</Title>
      {teams.map(team => (
        <div key={shortid.generate()}>
          <Button onClick={() => selectTeam(team.id)}>{team.name}</Button>
        </div>
      ))}
    </div>
  );
};

SelectTeam.propTypes = {
  teams: PropTypes.arrayOf(PropTypes.shape),
  selectTeam: PropTypes.func.isRequired,
};

SelectTeam.defaultProps = {
  teams: [],
};

export default SelectTeam;
