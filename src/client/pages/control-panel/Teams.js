import React from 'react';
import PropTypes from 'prop-types';
import shortid from 'shortid';
import {
  Icon,
  Button,
  Title,
  Table,
} from 'bloomer';

import TeamModal from './TeamModal';

class Teams extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalActive: false,
    };
    this.socket = props.socket;
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.addTeam = this.addTeam.bind(this);
    this.removeTeam = this.removeTeam.bind(this);
    this.removePoint = this.removePoint.bind(this);
    this.addPoint = this.addPoint.bind(this);
    this.toggleTeam = this.toggleTeam.bind(this);
  }

  openModal() {
    this.setState({
      modalActive: true,
    });
  }

  closeModal() {
    this.setState({
      modalActive: false,
    });
  }

  addTeam(name) {
    if (name) {
      this.socket.emit('add team', {
        name,
        id: shortid.generate(),
      });
      this.closeModal();
    }
  }

  removeTeam(name) {
    if (name) this.socket.emit('remove team', name);
  }

  addPoint(id) {
    this.socket.emit('add point', id);
  }

  removePoint(id) {
    this.socket.emit('remove point', id);
  }

  toggleTeam(id) {
    this.socket.emit('toggle team', id);
  }

  render() {
    const { teams } = this.props;
    const { modalActive } = this.state;


    return (
      <div>
        <TeamModal
          isActive={modalActive}
          closeModal={this.closeModal}
          addTeam={this.addTeam}
        />
        <Title isSize={5}>Lag</Title>
        <Table isStriped className="dashboard__teams">
          <thead>
            <tr>
              <th>Namn</th>
              <th className="dashboard__teams-points-th">Poäng</th>
              <th className="dashboard__teams-points-th">Dragit i bromsen</th>
              <th className="dashboard__teams-controls-th"><Icon className="fas fa-cog" /></th>
            </tr>
          </thead>
          <tbody>
            {teams.map(team => (
              <tr key={shortid.generate()}>
                <td>{team.name}</td>
                <td>
                  <div className="dashboard__teams-points-td">
                    <Button isSize="small" onClick={() => this.removePoint(team.id)}>
                      <Icon isSize="small" className="fas fa-minus" />
                    </Button>
                    <span className="dashboard__teams-points-td-counter">{team.points}</span>
                    <Button isSize="small" onClick={() => this.addPoint(team.id)}>
                      <Icon isSize="small" className="fas fa-plus" />
                    </Button>
                  </div>
                </td>
                <td className="dashboard__teams-controls-td">
                  <Button
                    onClick={() => this.toggleTeam(team.id)}
                  >
                    <span>{team.stopped.toString()}</span>
                  </Button>
                </td>
                <td className="dashboard__teams-controls-td">
                  <Button onClick={() => this.removeTeam(team.id)} isSize="small">
                    <Icon isSize="small" className="fas fa-trash" />
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
        <Button onClick={this.openModal}>
          <Icon className="fas fa-plus" />
        </Button>
      </div>
    );
  }
}

Teams.propTypes = {
  teams: PropTypes.arrayOf(PropTypes.shape),
  socket: PropTypes.object.isRequired, // eslint-disable-line 
};

Teams.defaultProps = {
  teams: [],
};

export default Teams;
