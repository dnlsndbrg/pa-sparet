import React from 'react';
import PropTypes from 'prop-types';
import shortid from 'shortid';

const Score = (props) => {
  const { teams } = props;
  return (
    <div className="score">
      {teams.map(team => (
        <div key={shortid.generate()}>
          <div className="name">
            {team.name && team.name.split('').map(letter => <span key={shortid.generate()}>{letter}</span>)}
          </div>
          <div className="points">{team.points}</div>
        </div>
      ))}
    </div>
  );
};

Score.propTypes = {
  teams: PropTypes.arrayOf(PropTypes.shape),
};

Score.defaultProps = {
  teams: [],
};

export default Score;
