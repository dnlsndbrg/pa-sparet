import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'bloomer';

const Stop = ({ stop }) => (
  <Button className="stop-button" onClick={stop}>Nödbroms</Button>
);

Stop.propTypes = {
  stop: PropTypes.func.isRequired,
};

export default Stop;
