import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import io from 'socket.io-client';

import ControlPanel from './pages/control-panel/ControlPanel';
import Play from './pages/Play';
import View from './pages/View';

class App extends Component {
  constructor() {
    super();
    this.state = {
      hasLoaded: false,
      teams: [],
      train: false,
      route: '',
    };
    this.socket = io(`${window.location.hostname}:3000`);
    this.socket.on('update', ({ teams, train, route }) => {
      this.setState({
        teams,
        train,
        route,
        hasLoaded: true,
      });
    });
  }

  render() {
    const {
      teams, train, route, hasLoaded,
    } = this.state;
    return (
      <Router>
        {
          hasLoaded
        && (
        <div className="App">
          <Route path="/" exact render={() => <Play teams={teams} train={train} socket={this.socket} />} />
          <Route path="/view" render={props => <View {...props} teams={teams} train={train} socket={this.socket} route={route} />} />
          <Route path="/control-panel" render={() => <ControlPanel socket={this.socket} teams={teams} train={train} route={route} />} />
        </div>
        )
        }
      </Router>
    );
  }
}

export default App;

// <Route path="/destination/:id" render={props => <Destination {...props} />} />
