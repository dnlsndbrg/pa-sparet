import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import shortid from 'shortid';
import {
  Container,
  Title,
  Subtitle,
  Hero,
  HeroBody,
  Button,
  Column,
  Columns,
  Box,
} from 'bloomer';

import Teams from './Teams';

const routes = [
  {
    name: 'Lobby',
    route: '/lobby',
  },
  {
    name: 'Resa 1',
    route: '/travel/destination-1',
  },
  {
    name: 'Resa 1 - Frågor',
    route: '/questions/destination-1',
  },
  {
    name: 'Resa 1 - Musik',
    route: '/music/destination-1',
  },
  {
    name: 'Resa 2',
    route: '/travel/destination-2',
  },
  {
    name: 'Resa 2 - Frågor',
    route: '/questions/destination-2',
  },
  {
    name: 'Resa 2 - Musik',
    route: '/music/destination-2',
  },
  {
    name: 'Resa 3',
    route: '/travel/destination-3',
  },
  {
    name: '3 - Frågor',
    route: '/questions/destination-3',
  },
  {
    name: 'Resa 3 - Musik',
    route: '/music/destination-3',
  },
];

function routeButtonClass(route, pageRoute) {
  console.log('-->>>>>>', route, pageRoute);
  return route === pageRoute ? 'button is-success' : '';
}

class ControlPanel extends React.Component {
  constructor(props) {
    super(props);
    this.socket = props.socket;
    this.toggleTrain = this.toggleTrain.bind(this);
    this.resetTeams = this.resetTeams.bind(this);
    this.navigate = this.navigate.bind(this);
  }

  toggleTrain() {
    this.socket.emit('toggle train');
  }

  resetTeams() {
    this.socket.emit('reset teams');
  }

  navigate(page) {
    this.socket.emit('navigate', page.route);
  }

  render() {
    const {
      teams, socket, train, route,
    } = this.props;
    return (
      <div className="dashboard">
        <Hero isColor="info">
          <Link to="/control-panel">
            <HeroBody>
              <Container>
                <Title>På spåret</Title>
                <Subtitle>Kontrollpanel</Subtitle>
              </Container>
            </HeroBody>
          </Link>
        </Hero>
        <main>
          <Columns isCentered>
            <Column isSize="2/4">
              <Box>
                <Title isSize={5}>Funktioner</Title>
                <Button onClick={this.toggleTrain}>
                  <span>{train ? 'Stoppa tåg' : 'Starta tåg'}</span>
                </Button>
                <Button
                  disabled={!teams.some(t => t.stopped)}
                  onClick={this.resetTeams}
                >
                  <span>Återställ alla lagbromsar</span>
                </Button>
              </Box>
              <Box>
                <Title isSize={5}>Navigering</Title>
                {routes.map(page => (
                  <Button
                    className={routeButtonClass(route, page.route)}
                    key={shortid.generate()}
                    onClick={() => this.navigate(page)}
                  >
                    <span>{page.name}</span>
                  </Button>
                ))}

              </Box>
            </Column>
            <Column isSize="2/4">
              <Box>
                <Teams socket={socket} teams={teams} />
              </Box>
            </Column>
          </Columns>
        </main>
      </div>
    );
  }
}

ControlPanel.propTypes = {
  teams: PropTypes.arrayOf(PropTypes.shape),
  socket: PropTypes.object.isRequired, // eslint-disable-line 
  train: PropTypes.bool.isRequired,
  route: PropTypes.string.isRequired,
};

ControlPanel.defaultProps = {
  teams: [],
};

export default ControlPanel;
