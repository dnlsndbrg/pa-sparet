import React from 'react';
import { Route } from 'react-router-dom';
import PropTypes from 'prop-types';


import Lobby from './Lobby';
import WhereTo from './destinations/WhereTo';
import Questions1 from './destinations/questions-1';
import Questions2 from './destinations/questions-2';
import Questions3 from './destinations/questions-3';


class View extends React.Component {
  constructor(props) {
    super(props);
    this.socket = props.socket;
    this.navigate = this.navigate.bind(this);
    this.socket.on('navigate', this.navigate);
    this.navigate(props.route);
  }

  navigate(route) {
    const { history, location } = this.props; /* eslint-disable-line */
    if (!location.pathname.includes(route)) {
      history.push(`/view${route}`);
    }
  }

  render() {
    const { teams, train } = this.props;
    return (
      <div className="view">
        <Route path="/view/lobby" exact render={() => <Lobby teams={teams} />} />
        <Route exact path="/view/travel/destination-1" render={() => <WhereTo videoId="H6Lm5mWO9VQ" train={train} />} />
        <Route exact path="/view/travel/destination-2" render={() => <WhereTo videoId="iTPhpRn4aj4" train={train} />} />
        <Route exact path="/view/travel/destination-3" render={() => <WhereTo videoId="MjAwC99X4aE" train={train} />} />
        <Route exact path="/view/questions/destination-1/" component={Questions1} />
        <Route exact path="/view/questions/destination-2/" component={Questions2} />
        <Route exact path="/view/questions/destination-3/" component={Questions3} />
      </div>
    );
  }
}

View.propTypes = {
  teams: PropTypes.arrayOf(PropTypes.shape),
  train: PropTypes.bool.isRequired,
  route: PropTypes.string.isRequired,
  socket: PropTypes.object.isRequired, // eslint-disable-line 
};
View.defaultProps = {
  teams: [],
};

export default View;
