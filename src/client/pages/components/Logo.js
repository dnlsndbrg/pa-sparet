import React from 'react';

import image from '../../assets/img/logo-dark.png';

const Logo = () => (
  <div className="logo" style={{ backgroundImage: `url(${image})` }} />
);


export default Logo;
