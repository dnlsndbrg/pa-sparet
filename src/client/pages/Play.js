import React from 'react';
import PropTypes from 'prop-types';
import SelectTeam from './components/SelectTeam';
import Stop from './components/Stop';

import Logo from './components/Logo';

function renderStop(id, teams, train) {
  const team = teams.filter(t => t.id === id && !t.stopped);
  if (!train || !id) return false; // train should be started and player should have selected a team
  if (team.length >= 1) return true; // player should not have already stopped
  return false;
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      teamId: null,
    };

    this.socket = props.socket;
    this.selectTeam = this.selectTeam.bind(this);
    this.stop = this.stop.bind(this);
  }

  selectTeam(teamId) {
    this.setState({
      teamId,
    });
  }


  stop() {
    const { teamId } = this.state;
    this.socket.emit('stop team', teamId);
  }


  render() {
    const { teams, train } = this.props;
    const { teamId } = this.state;
    return (
      <div className="play">
        {(teamId && !train) && <Logo /> }
        {!teamId && <SelectTeam teams={teams} selectTeam={this.selectTeam} />}
        { renderStop(teamId, teams, train) ? <Stop stop={this.stop} /> : null }
      </div>
    );
  }
}

Game.propTypes = {
  teams: PropTypes.arrayOf(PropTypes.shape),
  train: PropTypes.bool.isRequired,
  socket: PropTypes.object.isRequired, // eslint-disable-line 
};

Game.defaultProps = {
  teams: [],
};

export default Game;
