import React from 'react';
import {
  Subtitle, Columns, Column,
} from 'bloomer';

const Questions = () => (
  <div className="questions">
    <Columns>
      <Column isSize="1/2">
        <Subtitle hasTextColor="light" isSize={3}>Fråga 1</Subtitle>
        <p>
          Det här är konstgalleriet Santralistanbul, vad användes det till förr?
        </p>
        <hr />
      </Column>
      <Column isSize="1/2">
        <Subtitle hasTextColor="light" isSize={3}>Fråga 2</Subtitle>
        <p>
          Hizmet är en politisk rörelse som finns främst i Turkiet,
          men även i andra länder. Under vilket namn är rörelsen också
          känd och vad heter dess ledare? Bonuspoäng: i vilket land lever han i exil? (USA)
        </p>
        <hr />
      </Column>
    </Columns>
  </div>
);

export default Questions;
